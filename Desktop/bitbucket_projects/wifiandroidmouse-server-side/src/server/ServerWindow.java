package server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class ServerWindow implements ActionListener{
	
	private RemoteDataServer server;
	
	private Thread sThread;
	
	private static final int WINDOW_HEIGHT = 200;
	private static final int WINDOW_WIDTH = 350;
	
	private String ipAddress;
	
	private JFrame window = new JFrame("Android Wifi Mouse Server");
	
	private JLabel addressLabel = new JLabel("");
	private JLabel portLabel = new JLabel("PORT: ");
	private JTextArea[] buffers = new JTextArea[3];
	private JTextField portTxt = new JTextField(5);
	private JLabel serverMessages = new JLabel("Nije spojeno");
	
	private JButton connectButton = new JButton("Spoji");
	private JButton disconnectButton = new JButton("Odspoji");
	
	public ServerWindow(){
		server = new RemoteDataServer();
		
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		connectButton.addActionListener(this);
		disconnectButton.addActionListener(this);
		
		Container c = window.getContentPane();
		c.setLayout(new FlowLayout());
		
		try{
			InetAddress ip = InetAddress.getLocalHost();
			ipAddress = ip.getHostAddress();
			addressLabel.setText("IP Adresa: "+ipAddress);
		}
		catch(Exception e){addressLabel.setText("IP Addresu nije moguce pronaci");}
		
		int x;
		for(x = 0; x < 3; x++){
			buffers[x] = new JTextArea("", 1, 30);
			buffers[x].setEditable(false);
			buffers[x].setBackground(window.getBackground());
		}
		
		c.add(addressLabel);
		c.add(buffers[0]);
		c.add(portLabel);
		portTxt.setText("5444");
		c.add(portTxt);
		c.add(buffers[1]);
		c.add(connectButton);
		c.add(disconnectButton);
		c.add(buffers[2]);
		c.add(serverMessages);
		
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		window.setResizable(false);
	}
	
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		
		if(src instanceof JButton){
			if((JButton)src == connectButton){
				int port = Integer.parseInt(portTxt.getText());
				runServer(port);
			}
				
			else if((JButton)src == disconnectButton){
				closeServer();
			}
		}
	}
	
	public void runServer(int port){
		if(port <= 9999){
			server.setPort(port);
			sThread = new Thread(server);
			sThread.start();
		}
		else{
			serverMessages.setText("Broj porta mora biti manji od 10000");
		}
	}
	
	public void closeServer(){
		serverMessages.setText("Odspojeno");
		server.shutdown();
		connectButton.setEnabled(true);
	}
	
	public static void main(String[] args){
		new ServerWindow();
	}
	
	public class RemoteDataServer implements Runnable{
		int PORT;
		private DatagramSocket server;
		private byte[] buf;
		private DatagramPacket dgp;
		
		private String message;
		private Control bot;
		
		public RemoteDataServer(int port){
			PORT = port;
			buf = new byte[1000];
			dgp = new DatagramPacket(buf, buf.length);
			bot = new Control();
			serverMessages.setText("Nije spojeno");
		}
		
		public RemoteDataServer(){
			buf = new byte[1000];
			dgp = new DatagramPacket(buf, buf.length);
			bot = new Control();
			serverMessages.setText("Nije spojeno");
		}
		
		public String getIpAddress(){
			String returnStr;
			try{
					InetAddress ip = InetAddress.getLocalHost();
					returnStr = ip.getCanonicalHostName();
			}
			catch(Exception e){ returnStr = new String("Ne mogu pronaci IP adresu");}
			return returnStr;
		}
		
		public void setPort(int port){
			PORT = port;
		}
		
		public void shutdown(){
			try{server.close();
				serverMessages.setText("Odspojeno");}
			catch(Exception e){}
		}
		
		public void run(){
			boolean connected = false;
			try {InetAddress ip = InetAddress.getLocalHost(); 
				serverMessages.setText("Cekanje veze na " + ip.getCanonicalHostName());
				
				server = new DatagramSocket(PORT, ip);
				
				connected = true;
				connectButton.setEnabled(false);
			}
			catch(BindException e){ serverMessages.setText("Port "+PORT+" se vec koristi. Upotrijebite drugi port"); }
			catch(Exception e){serverMessages.setText("Ne mogu spojiti");}
			
			while(connected){
				try{ server.receive(dgp);
				
					message = new String(dgp.getData(), 0, dgp.getLength());
					if (message.equals("Connectivity")){
						serverMessages.setText("Pokusavam spojiti");
						server.send(dgp);
					}else if(message.equals("Connected")){
						server.send(dgp);
					}else if(message.equals("Close")){
						serverMessages.setText("Wifi Mouse je odspojen. Pokusavam spojiti.");
					}else{
						serverMessages.setText("Spojen na Wifi Mouse");
						bot.handleMessage(message);
					}
				}catch(Exception e){
					serverMessages.setText("Odspojen");
					connected = false;}
			}
		}
	}
}
