package com.example.androidwifimouse;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;

import com.example.androidwifimouse.AnalogueView.OnMoveListener;

import android.view.Menu;



public class Gamepad extends Activity implements OnKeyListener {
	
	int zadnja_pozicija_x;
	int zadnja_pozicija_y;
	
	
	public static final int DEFAULT_MODE = 10;
	public static final int DEFAULT_POWER = 0;
	public static int power_safe;
	
	
    int brojac = 1;
	int i;
	boolean keyboard = false;
	
	AnalogueView analogue;
    TextView showMoveEvent;

    String delim = new String("!!");
    
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
    	
    	
    	SharedPreferences sharedPreferences = getSharedPreferences ("MyOptions" , Context.MODE_PRIVATE);
    	int gamepad_mode = sharedPreferences.getInt("gamepad_mode" , DEFAULT_MODE);
    	int power_safe = sharedPreferences.getInt("power_safe" , DEFAULT_POWER);
    	
    	
    	if(gamepad_mode == 1)
    	{
    		Toast.makeText(this, "A-R mode is active", Toast.LENGTH_LONG).show();
    		AnalogueView.pozicija = 1;
    	}
    	else
    	{
    		Toast.makeText(this, "Normal mode is active", Toast.LENGTH_LONG).show();
    		AnalogueView.pozicija = 0;
    	}
    	
    	
    	if(power_safe == 0){
    	//sprjecava gasenje ekrana ukoliko je neaktivan
    	super.onCreate(savedInstanceState);
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	}
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gamepad_layout);

        
        Display display = getWindowManager().getDefaultDisplay(); 
	 	@SuppressWarnings("deprecation")
		int width = display.getWidth();
	 	
	 	Button left = (Button) findViewById(R.id.LeftClickButton);
	 	Button right =  (Button) findViewById(R.id.RightClickButton);
	 	
		left.setWidth(width/2);
	 	right.setWidth(width/2);
        
      
	
	 	
	 	 EditText editText = (EditText) findViewById(R.id.KeyBoard);
		    editText.setOnKeyListener(this);
		    editText.addTextChangedListener(new TextWatcher(){
			    public void  afterTextChanged (Editable s){
			    	Log.d("seachScreen", ""+s);
			    	s.clear();
		        } 
		        public void  beforeTextChanged  (CharSequence s, int start, int count, int after){ 
		                Log.d("seachScreen", "beforeTextChanged"); 
		        } 
		        public void  onTextChanged  (CharSequence s, int start, int before, int count) {
		        	Klijent appDel = ((Klijent)getApplicationContext());
		        	
		        	try{
		        		char c = s.charAt(start);
		        		appDel.sendMessage("KEY"+delim+c);
		        	}
		        	catch(Exception e){}
		        }
		    });
		    
	   
	   
	   
	   
	   
	    showMoveEvent = (TextView) findViewById(R.id.showMoveEvent);
        analogue = (AnalogueView) findViewById(R.id.analogueView1);
        analogue.setOnMoveListener(new OnMoveListener() {

        
   
        	
    @Override
    public void onMaxMoveInDirection(int b,double polarAngle, float smjerx , float smjery) {
        
    	  i = 1;
          onTouchEvent(null);
          
            StringBuilder sb = new StringBuilder();
            
            Log.d(String.valueOf(smjerx), String.valueOf(smjery));
            sb.append("MOVE"+delim);
       	 	sb.append((int)smjerx+delim);
			sb.append((int)smjery+delim);
            sendToAppDel(sb.toString());
           
            
          }
    
   
        
  @Override
  public void onHalfMoveInDirection(int a,double polarAngle,float smjerx , float smjery) {
				
	        i = 0;
			
			StringBuilder sb = new StringBuilder();
				
			Log.d("Zadnja pozicija", String.valueOf(smjery));
		
               zadnja_pozicija_x = (int) smjerx;
               zadnja_pozicija_y = (int) smjery;
               
              if(a==1)
              {  sb.append("DOWN"+delim);
				sb.append((int)smjerx+delim);
				sb.append((int)smjery+delim);   }
              else
              {
				    sb.append("MOVE"+delim);
					sb.append((int)smjerx+delim);
					sb.append((int)smjery+delim);
              }
						
					/*if(smjery<100)
					{
						
						
					}*/
					Log.d(String.valueOf(smjerx), String.valueOf(smjery));
					sendToAppDel(sb.toString());
			}
        });
    }
    
    
    
    public void onStart(){
		super.onStart();

		Klijent appDel = ((Klijent)getApplicationContext());
		sendToAppDel(new String("Mouse Sensitivity!!"+appDel.mouse_sensitivity));
		
			new Thread(new Runnable(){
				Klijent appDel = ((Klijent)getApplicationContext());
				public void run(){
					while(appDel.connected){
						try{Thread.sleep(500);}
						catch(Exception e){};
						if(!appDel.connected){
							finish();
						}
					}
				}
			}).start();
			
			
			 
		
			
	}
    
    @Override
	public boolean onKey(View v, int c, KeyEvent event){
		Log.d("ello", ""+event.getKeyCode());
		Klijent appDel = ((Klijent)getApplicationContext());
		
	 	appDel.sendMessage("S_KEY"+delim+event.getKeyCode());
		return false;
	}
    
    
	private void sendToAppDel(String message){
		Klijent appDel = ((Klijent)getApplicationContext());
		if(appDel.connected){
			appDel.sendMessage(message);
		}
		else{
			finish();
		}
	}
	
	

	
	
	
	  public void LeftButtonClickHandler(View v){
	    	Log.d("eloo", "CLICKED");
	    	sendToAppDel("CLICK"+delim+"LEFT");
	    }
	    
	    public void RightButtonClickHandler(View v){
	    	sendToAppDel("CLICK"+delim+"RIGHT");
	    }

	    public void keyClickHandler(View v){
	    	EditText editText = (EditText) findViewById(R.id.KeyBoard);
	    	InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    	
	    	if(keyboard){
	    		mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	    		keyboard = false;
	    	}
	    	else{
	    		mgr.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
	    		keyboard = true;
	    
	    	}
	    		
	    	Log.d("SET", "Foucs");
	    }
	    
	  
	
}