
package com.example.androidwifimouse;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MotionActivity extends Activity implements SensorEventListener, OnKeyListener, OnTouchListener{
	String delim = new String("!!");
	//int xi=0;
	
	float xi = 0;
	float yi=0;
	//int yi = 0;
		
	
	public static int motion_click;
	public static final int DEFAULT = 0;
	public static final int DEFAULT_POWER = 0;
	public static int power_safe;
	
	
	//int yi = (int) Math.sin(0);
	
	boolean keyboard = false;
	Thread checking;

	//TextView x, y;
	SensorManager sensorManager;
	Sensor s;
	int i = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		
		
		
		
		super.onCreate(savedInstanceState);
		
		
		SharedPreferences sharedPreferences = getSharedPreferences ("MyOptions" , Context.MODE_PRIVATE);
    	int motion_click = sharedPreferences.getInt("motion_click" , DEFAULT);
    	int power_safe = sharedPreferences.getInt("power_safe" , DEFAULT_POWER);
    	
    	
    	if(power_safe == 0){
    	//sprjecava gasenje ekrana ukoliko je neaktivan
    			super.onCreate(savedInstanceState);
    			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	}
    	
    	
   
		
	//	setContentView(R.layout.activity_motion);
		 setContentView(R.layout.control);
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		s = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	//	x = (TextView) findViewById(R.id.textView2);
	//	y = (TextView) findViewById(R.id.textView4);
		Button left = (Button) findViewById(R.id.LeftClickButton);
	 	Button right =  (Button) findViewById(R.id.RightClickButton);
	 	
		Display display = getWindowManager().getDefaultDisplay(); 
	 	@SuppressWarnings("deprecation")
		int width = display.getWidth();
	 	
		left.setWidth(width/2);
	 	right.setWidth(width/2);
	 	
	    View touchView = (View) findViewById(R.id.TouchPad);
	    touchView.setOnTouchListener(this);
	 	
	    EditText editText = (EditText) findViewById(R.id.KeyBoard);
	    editText.setOnKeyListener(this);
	    editText.addTextChangedListener(new TextWatcher(){
		    public void  afterTextChanged (Editable s){
		    	Log.d("seachScreen", ""+s);
		    	s.clear();
	        } 
	        public void  beforeTextChanged  (CharSequence s, int start, int count, int after){ 
	                Log.d("seachScreen", "beforeTextChanged"); 
	        } 
	        public void  onTextChanged  (CharSequence s, int start, int before, int count) {
	        	Klijent appDel = ((Klijent)getApplicationContext());
	        	
	        	try{
	        		char c = s.charAt(start);
	        		appDel.sendMessage("KEY"+delim+c);
	        	}
	        	catch(Exception e){}
	        }
	    });
	}
	
	public void onStart(){
		super.onStart();

		Klijent appDel = ((Klijent)getApplicationContext());
		sendToAppDel(new String("Mouse Sensitivity!!"+appDel.mouse_sensitivity));
		
			new Thread(new Runnable(){
				Klijent appDel = ((Klijent)getApplicationContext());
				public void run(){
					while(appDel.connected){
						try{Thread.sleep(1000);}
						catch(Exception e){};
						if(!appDel.connected){
							finish();
						}
					}
				}
			}).start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	@Override
	protected void onResume() {
		//super.onResume();
		//sensorManager.registerListener(this, s, SensorManager.SENSOR_MIN);
		super.onResume();
        sensorManager.registerListener(this, s, SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	//	getMenuInflater().inflate(R.menu.activity_motion, menu);
		return true;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		mousePadHandler(event);
	 	return true;
	}
	

	
    private void mousePadHandler(MotionEvent event) {
  	   StringBuilder sb = new StringBuilder();
  	
  	   /* za touch opcije*/
  	   
  	   if (motion_click == 1)
  	   {
  	   int action = event.getAction();
  	   int touchCount = event.getPointerCount();
  	   
  	   if(touchCount == 1){
 			switch(action){
 			//17.01
 			
 				case 0: sb.append("DOWN"+delim);
 						sb.append(xi+delim);
 						sb.append(yi+delim);
 						break;
 				
 				case 1: sb.append("UP"+delim);
 						sb.append(event.getDownTime()+delim);
 						sb.append(event.getEventTime());
 						break;
 				
 			
 			
 				default: break;
 			}
  	   }

  
  	   
  	  sendToAppDel(sb.toString());
  	}
    }

	public void onSensorChanged(SensorEvent event) {
		//if (i >= 3) {
		//	x.setText("" + event.values[0]);
		//	y.setText("" + event.values[1]);
		

			
		 	   StringBuilder sb = new StringBuilder();
		 	  sb.append("MOVE"+delim);
		 	  
		 	  
		 	  int x=(int)(event.values[0]);
		 	 int y=(int)(event.values[1]);
		 	 
		 /*	 if (x<0)
		 	 {
		 		xi=xi+4;
		 	 }
		 	 if (x>0)
		 	 {
		 		xi=xi-4;
		 	 }
		 	 
		 	 if (y<0)
		 	 {
		 		yi=yi-4;
		 	 }
		 	 if (y>0)
		 	 {
		 		yi=yi+4;
		 	 }
		 	 */
		 	 
		 	if (x<0)
		 	 {
		 		
		 		 //xi =- (int) event.values[1];
		 		xi=xi+1;
		 		//xi = xi + (int) event.values[0];
			 		
		 		
		 		
		 	 }
		 	 if (x>0)
		 	 {
		 		xi=xi-1;
		 		// xi =- (int) event.values[1];
		 		// xi = xi - event.values[0];
		 		//xi = xi - (int) event.values[0];
					 	 
		 	 }
		 	 
		 	 if (y<0)
		 	 {
		 		
		 		// yi = (int)event.values[1];
		 		 yi=yi+1;
		 		 
		 		//yi = yi + (int) event.values[1];
			 		
		 	 }
		 	if (y>0)
		 	 {
		 	 
		 		//22.1 provat s ovime jos!!
		 		// yi = (int) (yi - (int)  (Math.sin(180)+y));
		 		//cy = height/2;
		 		 
		 		// yi = (int) yi - (int) (10*Math.sin(45)+0.0001);
		 		
		 		//yi = (float) yi - (float) (2*Math.sin(45));
		 		 //yi = event.values[1];
		 		// yi = yi - event.values[1];
		 	
				 	// sendToAppDel(sb.toString());
		 		 
		 		// yi = yi - (int) event.values[1];
		 		yi=yi-1;
		 		
	
		 		
		 		Log.d(String.valueOf(yi), "fdsfs");
		 		
		 	 }
		 	 
		 	 sb.append((int)xi+delim);
		 	 sb.append((int)yi+delim);
			 // sb.append((int)event.getY());
				//sb.append(xi+delim);
				//sb.append(yi);
				
				 sendToAppDel(sb.toString());

			i=0;
		//}
		i++;
	}
	
	

    public void LeftButtonClickHandler(View v){
    	Log.d("eloo", "CLICKED");
    	sendToAppDel("CLICK"+delim+"LEFT");
    }
    
    public void RightButtonClickHandler(View v){
    	sendToAppDel("CLICK"+delim+"RIGHT");
    }

	
	private void sendToAppDel(String message){
		Klijent appDel = ((Klijent)getApplicationContext());
		if(appDel.connected){
			appDel.sendMessage(message);
		}
		else{
			finish();
		}
	}
	
  /*  public void keyClickHandler(View v){
    	EditText editText = (EditText) findViewById(R.id.KeyBoard);
    	InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    	
    	if(keyboard){
    		mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    		keyboard = false;
    	}
    	else{
    		mgr.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    		keyboard = true;
    	}
    		
    	Log.d("SET", "Foucs");
    }*/
    public void keyClickHandler(View v){
    	EditText editText = (EditText) findViewById(R.id.KeyBoard);
    	InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    	
    	if(keyboard){
    		mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    		keyboard = false;
    	}
    	else{
    		mgr.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    		keyboard = true;
    	}
    		
    	Log.d("SET", "Foucs");
    }

    @Override
	public boolean onKey(View v, int c, KeyEvent event){
		Log.d("ello", ""+event.getKeyCode());
		Klijent appDel = ((Klijent)getApplicationContext());
		
	 	appDel.sendMessage("S_KEY"+delim+event.getKeyCode());
		return false;
	}


	

}
